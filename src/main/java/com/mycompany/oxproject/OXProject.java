/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.mycompany.oxproject;

import java.util.Scanner;

/**
 *
 * @author ส้มส้ม
 */
public class OXProject {

    private char[][] board;
    
    public void printBoard() {
        System.out.println("|-----------|");
        for (int i = 0; i < 3; i++) {
            System.out.print("| ");
            for (int j = 0; j < 3; j++) {
                System.out.print(board[i][j] + " | ");
            }
            System.out.println();
            System.out.println("|-----------|");
        }
    }

    public void newBoard() {
        board = new char[3][3];
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                board[i][j] = '-';
            }
        }
    }
    
    
    public boolean checkDraw(){
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if(board[i][j] == '-'){
                    return false;
                }
            }
        }
        return true;
    }
    
    
    public boolean checkWin(char player){
        //check row
        for(int i=0; i<3; i++){
            if(board[i][0] == player && board[i][1]== player&& board[i][2]== player){
                return true;
            }
        }
        
        //check col
        for(int i=0; i<3; i++){
            if(board[0][i]== player&&board[1][i]== player && board[2][i]== player ){
                return true;
            }
        }
        
        //check diagonals
        if(board[0][0]== player&&board[1][1]== player && board[2][2]== player){
            return true;     
        }
        else if(board[0][2]== player&&board[1][1]== player && board[2][0]== player){
            return true;  
        }
        return false;
    }

    public void play() {
        Scanner kb = new Scanner(System.in);
        newBoard();
        char player1 = 'X';
        char player2 = 'O';
        System.out.println("|---------------------|");
        System.out.println("| Choose who go first |");
        System.err.println("|    Press 1 is X     |");
        System.err.println("|    Press 2 is O     |");
        System.out.println("|---------------------|");
        char player = kb.next().charAt(0);
        switch (player) {
            case '1' -> player = player1;
            case '2' -> player = player2;
            default -> {
                System.out.print("Choose again : ");
                player = kb.next().charAt(0);
            }
        }

        boolean gameEnd = false;
        printBoard();
        while (!gameEnd) {
            System.out.println("It's player " + player + " turn. Please input row[1-3] and column[1-3] for your move [ex:1 1]:");
            int row = kb.nextInt() - 1;
            int col = kb.nextInt() - 1;

            if (row >= 0 && row < 3 && col >= 0 && col < 3 && board[row][col] == '-') {
                board[row][col] = player;
                printBoard();
                if(checkWin('O')){
                    System.out.println("Congratulations! Player "+player+" wins!");
                    gameEnd = true;
                }else if(checkWin('X')){
                    System.out.println("Congratulations! Player "+player+" wins!");
                    gameEnd = true;
                }
                else if(checkDraw()){
                    System.out.println("The result is Draw!");
                    gameEnd = true;
                }   
                else{ 
                    player = (player == 'X')? 'O':'X';
                }
            } else {
                System.out.println("Invalid move. Please try again.");
            }
        }   
        System.out.print("Do you want to play again? (y/n) : ");
        String newGame = kb.next();
        if(newGame.equals("y")){
            play();
        }else{
           System.out.print("Bye!"); 
        }
    }

    public static void main(String[] args) {
        System.out.println("|--------------------|");
        System.out.println("| Welcome To OX Game |");
        System.out.println("|--------------------|");
        OXProject game = new OXProject();
        game.play();     
    }
}
